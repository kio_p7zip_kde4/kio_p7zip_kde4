/***************************************************************************
 *   Copyright (C) 2004 by Raul Fernandes                                  *
 *   rgfbr@yahoo.com.br                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef _p7zip_H_
#define _p7zip_H_


#include <kio/global.h>
#include <kio/slavebase.h>

class QByteArray;
class KProcess;
class KUrl;
class QString;
class QStringList;

class kio_p7zipProtocol : public QObject, public KIO::SlaveBase
{
Q_OBJECT
public:
    kio_p7zipProtocol(const QByteArray &pool_socket, const QByteArray &app_socket);
    virtual ~kio_p7zipProtocol();
    //virtual void mimetype(const KUrl& url);
    virtual void get(const KUrl& url);
    virtual void stat( const KUrl & url );
    virtual void listDir( const KUrl & url );
    virtual void del( const KUrl &url, bool isFile );
    virtual void put(const KUrl& url,int permissions, KIO::JobFlags);
protected:
    bool listArchive( const KUrl &archiveUrl );
    KProcess *proc1;
    KIO::filesize_t processed;
    KProcess *proc;
    QString p7zipProgram;
    KUrl archive;
    time_t archiveTime;
    QStringList archiveList;
public slots:
    void receivedData();
protected:
    bool checkName( const KUrl &url, KUrl &archiveUrl, KUrl &fileUrl );
};

#endif
