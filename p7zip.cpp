/***************************************************************************
 *   Copyright (C) 2004 by Raul Fernandes                                  *
 *   rgfbr@yahoo.com.br                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <qdatetime.h>
#include <qbitarray.h>
#include <qfile.h>

#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <kapplication.h>
#include <kdebug.h>
#include <kmessagebox.h>
#include <kcomponentdata.h>
#include <kglobal.h>
#include <kstandarddirs.h>
#include <klocale.h>
#include <kurl.h>

#include <kprocess.h>
#include <sys/stat.h>

#include "p7zip.moc"

using namespace KIO;


kio_p7zipProtocol::kio_p7zipProtocol(const QByteArray &pool_socket, const QByteArray &app_socket)
	: SlaveBase("kio_p7zip", pool_socket, app_socket)
{
	kDebug(7102) << "kio_p7zipProtocol::kio_p7zipProtocol()" << endl;
	p7zipProgram =  KGlobal::dirs()->findExe( "7za" );
	if( p7zipProgram.isNull() )// Check if 7za is available
		error( KIO::ERR_SLAVE_DEFINED, i18n( "The 7za binary was not found in PATH. You should install one of them to work with this kioslave" ) );
	archive = 0;
	archiveTime = 0;
}


kio_p7zipProtocol::~kio_p7zipProtocol()
{
	kDebug(7102) << "kio_p7zipProtocol::~kio_p7zipProtocol()" << endl;
}


void kio_p7zipProtocol::get(const KUrl& url )
{
	kDebug(7102) << "kio_p7zip::get(const KUrl& url)" << endl ;

	KUrl archiveUrl;
	KUrl fileUrl;
	if( !checkName( url, archiveUrl, fileUrl ) )
	{
		// it is not a 7zip archive
		// send the path to konqueror to redirect to file:/ kioslave
		redirection( url.path() );
		finished();
		return;
	}

	proc1 = new KProcess();
	processed = 0;
	connect( proc1, SIGNAL( readyRead() ), this, SLOT( receivedData() ) );

	QStringList args;
	args << "e" << "-so" << "-bd" << "-y" << archiveUrl.path() << fileUrl.path().remove( 0, 1 );

	infoMessage( i18n( "Unpacking file..." ) );
	proc1->setOutputChannelMode(KProcess::OnlyStdoutChannel);
	proc1->setNextOpenMode( QIODevice::ReadOnly | QIODevice::Unbuffered );
	proc1->setProgram( p7zipProgram, args );
	proc1->start();

	if (!proc1->waitForFinished())
		error( KIO::ERR_CANNOT_LAUNCH_PROCESS, url.path() );

	if( proc1->exitStatus() != QProcess::NormalExit )
		error( KIO::ERR_CANNOT_LAUNCH_PROCESS, url.path() );
	else if( proc1->exitStatus() != 0 )
		error( KIO::ERR_SLAVE_DEFINED, i18n( "A error occurred when unpacking %1" ).arg( url.path() ) );

	data( QByteArray() ); // empty array means we're done sending the data
	finished();

	delete proc1;
	proc1 = 0;
}


//void kio_p7zipProtocol::mimetype(const KUrl & /*url*/)
/*{
  mimeType("text/plain");
  finished();
  }*/


extern "C"
{
	int KDE_EXPORT kdemain(int argc, char **argv)
	{
		KComponentData instance( "kio_p7zip" );

		kDebug(7102) << "*** Starting kio_p7zip " << endl;

		if (argc != 4) {
			kDebug(7102) << "Usage: kio_p7zip  protocol domain-socket1 domain-socket2" << endl;
			exit(-1);
		}

		kio_p7zipProtocol slave(argv[2], argv[3]);
		slave.dispatchLoop();

		kDebug(7102) << "*** kio_p7zip Done" << endl;
		return 0;
	}
}


/*!
  \fn kio_p7zipProtocol::receivedData( KProcess *, char* buffer, int len )
  */
void kio_p7zipProtocol::receivedData()
{
	Q_ASSERT(proc1);
	if (!proc1->bytesAvailable())
	{
		return;
	}
	QByteArray temp = proc1->readAllStandardOutput();
	data(temp);
	processed += temp.length();
	processedSize( processed );
}


/*!
  \fn kio_p7zipProtocol::checkName( const KUrl &url, KUrl &archiveUrl, KUrl &fileUrl )
  */
bool kio_p7zipProtocol::checkName( const KUrl &url, KUrl &archiveUrl, KUrl &fileUrl )
{
	if( url.path().indexOf( ".7z", false ) == -1 )
		return false;
	archiveUrl = url.path().section( ".7z", 0, 0 ) + ".7z";
	if( url.path().endsWith( ".7z" ) )
		fileUrl = "/";
	else
		fileUrl = url.path().section( ".7z", 1 );
	return true;
}


/*!
  \fn kio_p7zipProtocol::stat( const KUrl & url )
  */
void kio_p7zipProtocol::stat( const KUrl & url )
{
	KUrl archiveUrl;
	KUrl fileUrl;
	if( !checkName( url, archiveUrl, fileUrl ) )
	{
		// it is not a 7zip archive
		// send the path to konqueror to redirect to file:/ kioslave
		redirection( url.path() );
		finished();
		return;
	}
	//messageBox( Information, QString( "stat\nurl: %1"/*\ndir: %2\nfileName: %3\nfile: %4"*/ ).arg( /*archiveName*/url.path() )/*.arg( dirName ).arg( fileName ).arg( file )*/ );

	UDSEntry entry;

	// check if it is root directory
	if( fileUrl.path() == "/" )
	{
		entry.insert(KIO::UDSEntry::UDS_NAME, "/");
		entry.insert(KIO::UDSEntry::UDS_FILE_TYPE, S_IFDIR);
		statEntry(entry);
		finished();
		return;
	}

	listArchive( archiveUrl );

	QString linha;

	const QString file = fileUrl.path( KUrl::RemoveTrailingSlash ).remove( 0, 1 );
	QString fileName;

	for( QStringList::Iterator it = archiveList.begin(); it != archiveList.end(); ++it )
	{
		linha = *it;
		if( linha.indexOf( "----" ) != -1 )
		{
			error( KIO::ERR_DOES_NOT_EXIST, url.path() );
			return;
		}
		fileName = linha.mid( 53 ).simplified();
		if( fileName == file ) break;
	}

	// name of file
	entry.insert( KIO::UDSEntry::UDS_NAME, fileName.section( '\\', -1 ) );

	QStringList list = linha.split( ' ', QString::SkipEmptyParts );

	// type of file
	fileName = list[ 2 ];
	entry.insert( KIO::UDSEntry::UDS_FILE_TYPE, fileName[ 0 ] == 'D' ? S_IFDIR : S_IFREG );

	// size
	entry.insert(KIO::UDSEntry::UDS_SIZE, list[ 3 ].toLong());

	statEntry(entry);
	finished();
}


/*!
  \fn kio_p7zipProtocol::listDir( const KUrl & url )
  */
void kio_p7zipProtocol::listDir( const KUrl & url )
{
	KUrl archiveUrl;
	KUrl fileUrl;
	if( !checkName( url, archiveUrl, fileUrl ) )
	{
		// it is not a 7zip archive
		// send the path to konqueror to redirect to file:/ kioslave
		redirection( url.path() );
		finished();
		return;
	}
	//messageBox( Information, QString( "listDir\nurl: %1\ndir: %2\nprogram: %3" ).arg( archiveUrl.path() ).arg( archiveUrl.path() ).arg( p7zipProgram ) );//.arg( fileName ).arg( file ) );

	infoMessage( i18n( "Listing directory..." ) );
	listArchive( archiveUrl );

	UDSEntry entry;
	int pos;
	QString linha;

	const QString file = fileUrl.path( KUrl::RemoveTrailingSlash ).remove( 0, 1 );
	QString fileName;

	for( QStringList::Iterator it = archiveList.begin(); it != archiveList.end(); ++it )
	{
		linha = *it;
		if( linha.indexOf( "----" ) != -1 ) break;

		entry.clear();

		fileName = linha.mid( 53 ).simplified();
		kDebug(7102) << "listDir\nline: " << fileName << endl;
		if( !file.isEmpty() )
		{
			if( fileName.indexOf( file ) == 0 )
			{
				if( fileName == file )
					continue;
				fileName = fileName.mid( file.length() + 1 );
			}
			else
				continue;
		}

		pos = fileName.indexOf( '/' );
		if ( pos != -1 )
			continue;

		// name of file
		entry.insert(KIO::UDSEntry::UDS_NAME, fileName.section( '/', -1 ));

		QStringList list = linha.split( ' ', QString::SkipEmptyParts );

		// type of file
		fileName = list[ 2 ];
		entry.insert( KIO::UDSEntry::UDS_FILE_TYPE, fileName[ 0 ] == 'D' ? S_IFDIR : S_IFREG);

		// size
		entry.insert(KIO::UDSEntry::UDS_SIZE, list[ 3 ].toLong());

		// time
		QDate date = QDate::fromString( list[ 0 ], "yyyy-MM-dd" );
		QTime time = QTime::fromString( list[ 1 ], "hh:mm:ss" );
		entry.insert(KIO::UDSEntry::UDS_MODIFICATION_TIME, QDateTime( date, time ).toTime_t());

		// send entry
		listEntry(entry, false);
	}

	listEntry( entry, true);
	finished();
}


/*!
  \fn kio_p7zipProtocol::put(const KUrl& url,int permissions,bool overwrite,bool resume)
  */
void kio_p7zipProtocol::put(const KUrl& url,int /*permissions*/, KIO::JobFlags)
{
	KUrl archiveUrl;
	KUrl fileUrl;
	if( !checkName( url, archiveUrl, fileUrl ) )
	{
		// it is not a 7zip archive
		// return the error that cannot enter the directory
		error(ERR_CANNOT_ENTER_DIRECTORY,url.path());
		return;
	}

	infoMessage( i18n( "Adding file..." ) );
	//messageBox( Information, QString( "put\nurl: %1\narchiveUrl: %2\nfileUrl: %3" ).arg( url.path() ).arg( archiveUrl.path() ).arg( fileUrl.path().remove( 0, 1 ) ) );

	// receive the data
	QByteArray buffer;
	int length;
	QFile file( "/tmp/" + fileUrl.fileName() );
	file.open( QIODevice::WriteOnly );
	do
	{
		dataReq();
		length = readData( buffer );
		file.write( buffer );
	}
	while( length > 0 );
	file.close();

	proc = new KProcess();
	QStringList args;
	args << "a" << archiveUrl.path() << file.fileName();
	proc->execute( p7zipProgram, args );

	if( proc->exitStatus() != QProcess::NormalExit )
		error( KIO::ERR_CANNOT_LAUNCH_PROCESS, url.path() );
	else if( proc->exitStatus() != 0 )
		error( KIO::ERR_SLAVE_DEFINED, i18n( "A error occurred when adding %1" ).arg( url.path() ) );

	//delete the temporary file
	file.remove();

	delete proc;
	proc = 0;

	finished();
}


/*!
  \fn kio_p7zipProtocol::listArchive( const KUrl &archiveUrl )
  */
bool kio_p7zipProtocol::listArchive( const KUrl &archiveUrl )
{
	if( archive == archiveUrl )
	{  // Has it changed ?
		struct stat statbuf;
		if ( ::stat( QFile::encodeName( archiveUrl.path() ), &statbuf ) == 0 )
		{
			if ( archiveTime == statbuf.st_mtime )
				return true;
		}
	}
	archive = archiveUrl;
	proc = new KProcess();
	QStringList args;
	args << "l" << archive.path();
	proc->setProgram( p7zipProgram, args );
	proc->setOutputChannelMode(KProcess::MergedChannels);
	proc->setNextOpenMode( QIODevice::ReadOnly | QIODevice::Unbuffered );
	proc->start();
	if( !proc->waitForFinished() )
		error( KIO::ERR_CANNOT_LAUNCH_PROCESS, archive.path() );

	archiveList.clear();
	QString linha;
	while( proc->canReadLine() )
	{
		linha = proc->readLine();
		if( linha.indexOf( "----" ) != -1 ) break;
	}

	while( proc->canReadLine() )
	{
		linha = proc->readLine();
		if( linha.indexOf( "----" ) != -1 ) break;
		archiveList.append( linha );
	}

	delete proc;
	proc = 0;
	return true;
}


/*!
  \fn kio_p7zipProtocol::del( const KUrl &url, bool isFile )
  */
void kio_p7zipProtocol::del( const KUrl &url, bool /*isFile*/ )
{
	KUrl archiveUrl;
	KUrl fileUrl;
	if( !checkName( url, archiveUrl, fileUrl ) )
	{
		// it is not a 7zip archive
		// return the error that cannot enter the directory
		error(ERR_CANNOT_ENTER_DIRECTORY,url.path());
		return;
	}

	infoMessage( i18n( "Deleting file..." ) );
	//messageBox( Information, QString( "del\nurl: %1\narchiveUrl: %2\nfileUrl: %3" ).arg( url.path() ).arg( archiveUrl.path() ).arg( fileUrl.path().remove( 0, 1 ) ) );

	proc1 = new KProcess();
	QStringList args;
	args << "d" << archiveUrl.path() << fileUrl.path().remove( 0, 1 );
	proc1->execute( p7zipProgram, args );

	if( proc1->exitStatus() != QProcess::NormalExit )
		error( KIO::ERR_CANNOT_LAUNCH_PROCESS, url.path() );
	else if( proc1->exitCode() != 0 )
		error( KIO::ERR_SLAVE_DEFINED, i18n( "A error occurred when deleting %1" ).arg( url.path() ) );

	finished();
}
