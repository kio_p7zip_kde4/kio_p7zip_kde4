/* This file is part of the KDE project
 * Copyright (C) 2005 Raul Fernandes
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation version 2.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#include <config.h>
#include "kfile_7zip.h"

#include <kgenericfactory.h>
#include <kio/global.h>
#include <kprocio.h>
#include <kstandarddirs.h>

#include <qfile.h>

typedef KGenericFactory<K7zipPlugin> K7zipFactory;

K_EXPORT_COMPONENT_FACTORY( kfile_7zip, K7zipFactory( "kfile_7zip" ) )

K7zipPlugin::K7zipPlugin(QObject *parent, const char *name, const QStringList &args)
           : KFilePlugin(parent, name, args)
{
    KFileMimeTypeInfo* info = addMimeTypeInfo( "application/x-7z" );

    KFileMimeTypeInfo::GroupInfo* group = 0L;

    group = addGroupInfo(info, "7zipInfo", i18n("7zip Information"));

    KFileMimeTypeInfo::ItemInfo* item;

    item = addItemInfo(group, "Items", i18n("Items"), QVariant::UInt);
    item = addItemInfo(group, "Size", i18n("Size"), QVariant::ULongLong);
    setUnit(item, KFileMimeTypeInfo::Bytes);
    item = addItemInfo(group, "Packed", i18n("Packed"), QVariant::ULongLong);
    setUnit(item, KFileMimeTypeInfo::Bytes);
    //item = addItemInfo(group, "Ratio", i18n("Ratio"), QVariant::String);
}

bool K7zipPlugin::readInfo( KFileMetaInfo& info, uint /*what*/)
{
    KFileMetaInfoGroup group = appendGroup(info, "7zipInfo");

    if ( info.path().isEmpty() )
       return false;

    if ( !QFile::exists( info.path() ) )
       return false;

    QString program = KGlobal::dirs()->findExe( "7za" );
    if( program.isNull() )
       return false;

    KProcIO proc;
    QString line;
    proc << program << "l" << info.path();
    proc.start( KProcess::Block );
    if ( !proc.normalExit() || proc.exitStatus() != 0 )
       return false;
    do
    {
      proc.readln( line );
    }while( line.find( "-----" ) == -1 );

    do
    {
      proc.readln( line );
    }while( line.find( "-----" ) == -1 );

    proc.readln( line );
    QStringList list = QStringList::split( ' ', line );

    appendItem( group, "Items", list[2].toUInt() );
    appendItem( group, "Size", list[0].toULongLong() );
    appendItem( group, "Packed", list[1].toULongLong() );
    //appendItem( group, "Ratio", list[3] );

    return true;
}

#include "kfile_7zip.moc"
